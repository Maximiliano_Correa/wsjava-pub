package newpackage;

public class LibroCalificaciones
{
   private String nombreDelCurso; // nombre del curso para este LibroCalificaciones

   // el constructor inicializa nombreDelCurso con un argumento String
   public LibroCalificaciones( String nombre ) // el nombre del constructor es el nombre de la clase
   {
      nombreDelCurso = nombre; // inicializa nombreDelCurso
   } // fin del constructor

   // m�todo para establecer el nombre del curso
   public void establecerNombreDelCurso( String nombre )
   {
      nombreDelCurso = nombre; // almacena el nombre del curso
   } // fin del m�todo establecerNombreDelCurso

   // m�todo para obtener el nombre del curso
   public String obtenerNombreDelCurso()
   {
      return nombreDelCurso;
   } // fin del m�todo obtenerNombreDelCurso

   // muestra un mensaje de bienvenida al usuario de LibroCalificaciones
   public void mostrarMensaje()
   {
      // esta instrucci�n llama a obtenerNombreDelCurso para obtener el 
      // nombre del curso que este LibroCalificaciones representa
      System.out.printf( "Bienvenido al Libro de calificaciones para\n%s!\n", 
         obtenerNombreDelCurso() );
   } // fin del m�todo mostrarMensaje
} // fin de la clase LibroCalificaciones
