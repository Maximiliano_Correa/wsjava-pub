// Incremento.java
// Operadores de preincremento y postincremento.

public class Incremento 
{
   public static void main( String[] args )
   {
      int c;
   
      // demuestra el operador de postincremento
      c = 5; // asigna 5 a c
      System.out.println( c );   // imprime 5
      System.out.println( c++ ); // imprime 5, despu�s postincrementa
      System.out.println( c );   // imprime 6

      System.out.println(); // omite una l�nea

      // demuestra el operador de preincremento
      c = 5; // asigna 5 a c
      System.out.println( c );   // imprime 5
      System.out.println( ++c ); // preincrementa y despu�s imprime 6
      System.out.println( c );   // imprime 6
   } // fin de main
} // fin de la clase Incremento
