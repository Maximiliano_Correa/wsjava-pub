// Calcular.java
// Calcula la suma de los enteros del 1 al 10 
public class Calcular 
{
   public static void main( String[] args )
   {
      int suma;
      int x;

      x = 1;   // inicializa x en 1 para contar
      suma = 0; // inicializa suma en 0 para el total

      while ( x <= 10 ) // mientras que x sea menor o igual que 10      
      {
         suma += x; // suma x a suma
         ++x; // incrementa x
      } // fin de while

      System.out.printf( "La suma es: %d\n", suma );
   } // fin de main
} // fin de la clase Calcular

