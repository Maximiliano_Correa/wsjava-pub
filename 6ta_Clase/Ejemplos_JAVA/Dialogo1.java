// Fig. 3.17: Dialogo1.java
// Uso de JOptionPane para imprimir varias l�neas en un cuadro de di�logo.
import javax.swing.JOptionPane; // importa la clase JOptionPane

public class Dialogo1
{
   public static void main( String[] args )
   {
      // muestra un cuadro de di�logo con un mensaje 
      JOptionPane.showMessageDialog( null, "Bienvenido\na\nJava" );
   } // fin de main
} // fin de la clase Dialogo1

