// Cuenta.java
// La clase Cuenta con un constructor para validar e
// inicializar la variable de instancia saldo de tipo double.

public class Cuenta
{   
   private double saldo; // variable de instancia que almacena el saldo

   // constructor  
   public Cuenta( double saldoInicial )
   {
      // valida que saldoInicial sea mayor que 0.0; 
      // si no lo es, saldo se inicializa con el valor predeterminado 0.0
      if ( saldoInicial > 0.0 ) 
         saldo = saldoInicial; 
   } // fin del constructor de Cuenta

   // abona (suma) un monto a la cuenta
   public void abonar( double monto )
   {      
      saldo = saldo + monto; // suma el monto al saldo 
   } // fin del m�todo abonar

   // devuelve el saldo de la cuenta
   public double obtenerSaldo()
   {
      return saldo; // proporciona el valor de saldo al m�todo que hizo la llamada
   } // fin del m�todo obtenerSaldo
} // fin de la clase Cuenta

