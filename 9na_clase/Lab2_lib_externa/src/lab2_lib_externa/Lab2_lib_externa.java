/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2_lib_externa;
import trigonometria.basica.*;
/**
 *
 * @author anonimo
 */
public class Lab2_lib_externa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Punto p1 = new Punto(2,1);
        Punto p2 = new Punto(-1,3);
        
        System.out.printf("-------------------Acividad 1-------------------\n"
                + "\nLa distancia entre el punto 1 y punto 2 es: %.2f\n",p1.calcularDistancia(p2));
        
        //Actividad 2
        Circulo circulo = new Circulo(5,3,3);
        
        System.out.printf("-------------------Acividad 2-------------------\n"
                + "\nEl area del circulo es: %.2f\n",circulo.calcularArea());
        
        System.out.printf("El perimetro del circulo es: %.2f\n",circulo.calcularPerimetro());
        
        System.out.printf("La distancia entre el centro del circulo y el punto 2 es: %.2f\n", circulo.calcularDistancia(p2));
        
        
        //Actividad 3
        Triangulo triangulo = new Triangulo(1,2,1,5,6,2);
        
        System.out.printf("-------------------Acividad 3-------------------\n"
                + "\nEl area del triangulo es: %.2f\n",triangulo.calcularArea());
        
        System.out.printf("El perimetro del triangulo es: %.2f\n",triangulo.calcularPerimetro());
        
        Punto p3 = new Punto(2,4);
        System.out.printf("La distancia entre el centro del triangulo y el punto 3 es: %.2f\n", triangulo.calcularDistancia(p3));


    }
    
}
