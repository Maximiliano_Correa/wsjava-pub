
public class racionales { 
		//atributos  
	 private long nume;
	 private long denom;
	  
	 	//constructor por defecto de la clase 
	 public  racionales(){
		nume=0;denom=1;
	}
	 	//constructor que si ingresa un elemento entero
	 public racionales(long elem){
	  nume= elem; denom=1;
	 }
	 
	 	//constructor cuando te ingresan numerador y denominador
	 public racionales(long numerador, long denominador){
		 nume=numerador;
		 denom=denominador;
	 }
	 	

//////////////////////////////////////////////////////////////////////////// 
//Comienzo de la clase racionales 
 
		//________________________________________________________________
		 //suma de racionales 
		 public  racionales sumaRac(racionales o){
		  racionales resultado = new racionales (this.nume * o.denom + this.denom*o.nume,
					this.denom * o.denom);	 
		 return resultado;
		 }
		
		//_________________________________________________________________
		//resta de racionales 
		 public racionales restaRac(racionales h){
			 racionales resultado=new racionales (this.nume*h.denom - this.denom*h.nume,
					 this.denom * h.denom);
			 return resultado;
		 }
		//________________________________________________________________
		// multilicacion de racionales
		 public racionales mulRac(racionales g){
			 racionales resultado = new racionales (this.nume*g.nume,this.denom*g.denom);
			 return resultado;
		 }
		//_________________________________________________________________
		// division de racionales 
		 public racionales divi(racionales b){
			racionales resultado=new racionales (this.nume*b.denom,this.denom*b.nume);
			return resultado;
		 }
		//__________________________________________________________________ 
		 // igualdada de racionales
		 public boolean iguales(racionales a){
			 return (this.nume == a.nume) && (this.denom == a.denom);
		 }
		 //__________________________________________________________________
		 //metodo ToString
	     public String toString(){	 
		 String res = new String ("["+nume+ "/" +denom+"]"); 
		 return res;
		 }
		////////////////////////////////////////////////////////////////////////////
		 //Fin de la clase racionales
} 

